const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, '/public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: 'inst'
    },
    jwt: {
        secret: 'very_secret_key',
        expires: '7d'
    }
};

