const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
  title: {
    type: String,
    required: true,
    unique: true
  },
  description: String
});

const Comments = mongoose.model('Comments', CategorySchema);

module.exports = Comments;