import React from "react";
import { createStackNavigator } from "react-navigation";
import { Image } from "react-native";
import NavButton from "../components/NavButton/NavButton";
import sharedRoutes, { sharedOptions } from "./sharedRoutes";
import PostsScreen from "../screens/PostsScreen/PostsScreen";

const HomeRoute = createStackNavigator(
	{
		Home: {
			screen: PostsScreen,
			navigationOptions: ({navigation}) => ({
				headerTitle: (
					<Image
						source={require("../assets/images/logo.png")}
						style={{height: 35}}
						resizeMode={"contain"}
					/>
				),
				headerLeft: (
					<NavButton
						iconName={"ios-camera-outline"}
						onPress={() => navigation.navigate("TakePhoto")}
					/>
				),
				headerRight: (
					<NavButton
						iconName={"ios-send-outline"}
						onPress={() => (null)}
					/>
				)
				
			})
		},
		...sharedRoutes
	},
	{
		...sharedOptions
	}
);

export default HomeRoute;