import React, { Component } from "react";
import { Text, View } from "react-native";

class ProfileDetailScreen extends Component {
	static navigationOptions = ({ navigation }) => ({
		title: navigation.state.params.user.username
	});
	
	render() {
		return (
			<View><Text>Profile</Text></View>
		);
	}
}

export default ProfileDetailScreen;
