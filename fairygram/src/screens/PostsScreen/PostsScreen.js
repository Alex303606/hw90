import React, { Component } from "react";
import { connect } from 'react-redux';
import Post from "../../components/Post/Post";
import {
	View,
	ScrollView,
	RefreshControl,
	StyleSheet
} from "react-native";
import { getAllPosts } from "../../store/actions/posts";

class PostsScreen extends Component {
	componentDidMount() {
		if (this.props.user) this.props.getAllPosts();
	}
	
	render() {
		console.log(this.props.posts);
		return (
			<ScrollView
				refreshControl={
					<RefreshControl
						refreshing={false}
						//onRefresh={props.refresh}
						tintColor={"black"}
					/>
				}
			>
				<View style={styles.container}>
					{this.props.posts && this.props.posts.map(post => <Post {...post} key={post._id}/>)}
				</View>
			</ScrollView>
		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white"
	}
});

const mapStateToProps = state => ({
	posts: state.posts.posts,
	user: state.user.user
});

const mapDispatchToProps = dispatch => ({
	getAllPosts: () => dispatch(getAllPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(PostsScreen);
